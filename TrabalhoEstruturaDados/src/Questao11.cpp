#include "Questao11.h"

Questao11::Questao11()
{
    //ctor
}


void inicializarVetor(int tamanho){
int* vetor[tamanho];

	for (int i=0; i < tamanho; i++){
		vetor[i] = new int;
		*(vetor[i]) = 0;
	}
}

void lerVetor(int tamanho){
int* vetor[tamanho];

	*vetor[0] = -5;
	*vetor[1] = 6;
	*vetor[2] = 0;
	*vetor[3] = 4;
	*vetor[4] = 22;
	*vetor[5] = 18;
	*vetor[6] = 48;
	*vetor[7] = 233;
	*vetor[8] = -411;
	*vetor[9] = -11;

}

void imprimirVetor(int tamanho, int operacao){
int* vetor[tamanho];

	switch( operacao ){
		case 1:
			cout << endl << endl << "|Valores ordenados Selection Sort: ";
			break;
		case 2:
			cout << endl << endl << "|Valores ordenados Insertion Sort: ";
			break;
		case 3:
			cout << endl << endl << "|Valores ordenados Bubble Sort: ";
			break;
		default:
			cout << "Opcao Invalida";
			exit(1);
			break;
	}

	cout << "\n|------------------------------|\n";
	for (int i=0; i < tamanho; i++){
        cout << "|Posicao "<< (i+1) <<": "<< *vetor[i] << endl;
    }
    cout << "\n|------------------------------|\n";
}

void selectionSort(int tamanho){
    int* vetor[tamanho];

    for (int i=0; i < tamanho; i++){
        int menor = i;

		for (int j=i+1; j < tamanho; j++){
            if (*vetor[j] < *vetor[menor]){
                menor = j;
            }
        }

		if (i != menor){
            int temp = *vetor[i];
            *vetor[i] = *vetor[menor];
            *vetor[menor] = temp;
        }
    }

    imprimirVetor(tamanho,1);
}

void insertionSort(int tamanho){
    int* vetor[tamanho];

    for (int i=1; i<tamanho; i++){
        int comp = *vetor[i];
        int j = i - 1;

		for (; j>=0 && comp < *vetor[j]; j--){
            *vetor[j+1] = *vetor[j];
        }

		*vetor[j+1] = comp;
    }

    imprimirVetor(tamanho,2);
}

void bubbleSort(int tamanho){
    int* vetor[tamanho];

	int aux;

	for(int j=tamanho-1; j>=1; j--){

		for(int i=0; i<j; i++){

			if(*vetor[i] > *vetor[i+1]) {

				aux = *vetor[i];
				*vetor[i] = *vetor[i+1];
				*vetor[i+1]= aux;
			}
		}
	}

	imprimirVetor(tamanho,3);
}

void escolherOpcao(int tamanho){
	int operacao;

	cout << "Escolha o metodo de ordenacao:\n  1- Selection Sort\n  2- Insertion Sort\n  3- Bubble Sorte\n -> ";
	cin >> operacao;

	switch( operacao ){
		case 1:
			selectionSort(tamanho);
			break;
		case 2:
			insertionSort(tamanho);
			break;
		case 3:
			bubbleSort(tamanho);
			break;
		default:
			cout << "Opcao Invalida";
			break;
	}

}
