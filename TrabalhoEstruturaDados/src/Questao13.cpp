#include "Questao13.h"

Questao13::Questao13()
{

}

int** inicializar(int tamanho){
    int** vet = new int*[tamanho];
    for (int i=0; i < tamanho;i++){
        vet[i]=new int;
    }
    return vet;
}

void lerVetor(int *vetor[],int tamanho){
    for (int i=0; i< tamanho; i++){
        if (vetor[i]!=0){
          cout << "Posicao "<<(i+1)<<": ";
          cin >> *(vetor[i]);
        }
    }
}


void imprimirOrdenadoBubble(int **vetor,int tamanho){
    int aux;

	for(int j=tamanho-1; j>=1; j--){

		for(int i=0; i<j; i++){

			if(*vetor[i] > *vetor[i+1]) {

				aux = *vetor[i];
				*vetor[i] = *vetor[i+1];
				*vetor[i+1]= aux;
			}
		}
	}

	cout << endl << endl << "|Valores ordenados Bubble Sort: ";

	cout << "\n|------------------------------|\n";
	for (int i=0; i < tamanho; i++){
        cout << "|Posicao "<< (i+1) <<": "<< *vetor[i] << endl;
    }
    cout << "\n|------------------------------|\n";

}

